import { expect } from 'chai';
import * as Fs from 'fs';
import * as Path from 'path';

import { SleeperClient } from '../../src/Clients/SleeperClient';
import { TrendingType } from '../../src/models/players/TrendingType';

const outputDirectory = 'data';
const sleeper = new SleeperClient();

describe('PlayersClient..', () => {
    it.only('gets all players in the league', async () => {
        const players = await sleeper.players.getAllPlayersAsync();
        console.log(players)

        expect(players.find(p => p.player_id === 1000).search_full_name).to.equal('jiizuke');
    })
    it('gets all trending players', async () => {
        const trendingPlayers = await sleeper.players.getTrendingPlayersAsync(TrendingType.ADD);

        expect(trendingPlayers).to.not.be.empty;
    })
})