import { expect } from 'chai';
import * as Fs from 'fs';
import * as Path from 'path';

import { SleeperClient } from '../../src/Clients/SleeperClient';

const avatarId = 'b6f7eee9c000b1d41716eb7fa8608042';
const outputDirectory = 'images';
const sleeper = new SleeperClient();

describe('AvatarClient..', () => {
    it('gets full profile picture', async () => {
        const fileName = await sleeper.avatar.getProfilePictureAsync(avatarId, true);
        const path = Path.resolve(outputDirectory, fileName);

        expect(Fs.existsSync(path)).to.be.true;
    }),
    it('gets thumbnail', async () => {
        const fileName = await sleeper.avatar.getProfilePictureAsync(avatarId, false);
        const path = Path.resolve(outputDirectory, fileName);

        expect(Fs.existsSync(path)).to.be.true;
    })
})