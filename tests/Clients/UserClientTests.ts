import { expect } from 'chai';

import { SleeperClient } from '../../src/Clients/SleeperClient';

const username = 'saavy1';
const userId = '656648916330188800';
const sleeper = new SleeperClient();

describe('UserClient..', () => {
    it('gets user by username', async () => {
        const user = await sleeper.user.getAsync(username);

        expect(user).to.not.be.null;
        expect(user.username).to.equal(username);
    }),
    it('gets user by id', async () => {
        const user = await sleeper.user.getAsync(userId);

        expect(user).to.not.be.null;
        expect(user.user_id).to.equal(userId);
    })
})