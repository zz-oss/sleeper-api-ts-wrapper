import { expect } from 'chai';

import { SleeperClient } from '../../src/Clients/SleeperClient';

const draftId = '656648987104841728';
const sleeper = new SleeperClient();

describe('DraftClient..', () => {
    it('gets a specific draft by id', async () => {
        const draft = await sleeper.draft.getDraftAsync(draftId);

        expect(draft.draft_id).to.equal(draftId);
        expect(draft.status).to.equal('complete');
    })
    it('gets all picks in a specific draft', async () => {
        const picks = await sleeper.draft.getPicksInDraft(draftId);

        expect(picks.length).to.equal(30);
        expect(picks[0].metadata.username).to.equal('Perkz')
    })
    it('gets all traded picks in a specific draft', async () => {
        const tradedPicks = await sleeper.draft.getTradedPicksInDraft(draftId);

        expect(tradedPicks.length).to.equal(0);
    })
})