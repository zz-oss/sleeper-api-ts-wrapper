import { expect } from 'chai';

import { SleeperClient } from '../../src/Clients/SleeperClient';
import { PlayerViewModel } from '../../src/models/players/PlayerViewModel';

const leagueId = '656648984051425280';
const week = 1;
const sleeper = new SleeperClient();

describe('LeagueClient..', () => {
    it('gets a specific league by id', async () => {
        const league = await sleeper.league.getLeagueAsync(leagueId);

        expect(league.name).to.equal('The Boys')
    })
    it('gets all the rosters in a league', async () => {
        const rosters = await sleeper.league.getRostersAsync(leagueId);

        expect(rosters.length).to.equal(6);
    })
    it('gets all users in a league', async () => {
        const leagueUsers = await sleeper.league.getLeagueUsersAsync(leagueId);

        expect(leagueUsers.length).to.equal(6);
    })
    it('gets all matchups in a league for a specific week', async () => {
        const matchups = await sleeper.league.getMatchupsAsync(leagueId, week);

        expect(matchups.length).to.equal(6);
        expect(matchups[0].points).to.equal(557.51);
    })
    it('gets all winners bracket matches', async () => {
        const winnersBracket = await sleeper.league.getWinnersBracketAsync(leagueId);

        expect(winnersBracket.length).to.equal(4);
    })
    it('gets all losers bracket matches', async () => {
        const losersBracket = await sleeper.league.getLosersBracketAsync(leagueId);

        expect(losersBracket.length).to.equal(1);
    })
    it('gets all playoff bracket matches', async () => {
        const fullBracket = await sleeper.league.getFullPlayoffsBracketAsync(leagueId);

        expect(fullBracket.length).to.equal(5);
    })
    it('gets all transaction for a specific week', async () => {
        const transactions = await sleeper.league.getTransactionsAsync(leagueId, week);

        expect(transactions.length).to.equal(1);
        expect(transactions[0].status).to.equal('complete');
    })
    it('gets all drafts for a specific league', async () => {
        const drafts = await sleeper.league.getDraftsAsync(leagueId);

        expect(drafts.length).to.equal(1);
        expect(drafts[0].status).to.equal('complete');
    })
    it('gets the latest messages', async () => {
        const messages = await sleeper.league.getMessagesAsync(leagueId);

        expect(messages).to.not.be.empty;
    })
    it('gets the latest league report', async () => {
        const leagueReports = await sleeper.league.getLeagueReport(leagueId);

        expect(leagueReports).to.not.be.null;
    })
})