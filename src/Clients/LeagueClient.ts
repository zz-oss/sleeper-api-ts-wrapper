import { DraftViewModel } from '../models/drafts/DraftViewModel';
import { LeagueMatchupsViewModel } from '../models/leagues/LeagueMatchupsViewModel';
import { LeagueReport } from '../models/leagues/report/LeagueReport';
import { LeagueRosterViewModel } from '../models/leagues/LeagueRosterViewModel';
import { LeagueTransactionsViewModel } from '../models/leagues/LeagueTransactionsViewModel';
import { LeagueUsersViewModel } from '../models/leagues/LeagueUsersViewModels';
import { LeagueViewModel } from '../models/leagues/LeagueViewModel';
import { PlayoffBracketViewModel } from '../models/leagues/PlayoffBracketViewModel';
import { BaseClient } from './BaseClient';
import { LeagueMessage } from '../models/leagues/LeagueMessage';

export class LeagueClient extends BaseClient {
    private readonly leaguePath: string;
    private readonly graphQLPath: string;
    constructor() {
        super();

        this.leaguePath = '/v1/league';
        this.graphQLPath = '/graphql'
    }

    async getLeagueAsync(leagueId: string): Promise<LeagueViewModel> {
        const response = await this.sleeper.get(`${this.leaguePath}/${leagueId}`);
        const league = response.data;

        return league;
    }

    async getRostersAsync(leagueId: string): Promise<LeagueRosterViewModel[]> {
        const response = await this.sleeper.get(`${this.leaguePath}/${leagueId}/rosters`)
        const rosters = response.data;

        return rosters;
    }

    async getRosterByIdAsync(leagueId: string, rosterId: number): Promise<LeagueRosterViewModel> {
        const rosters: LeagueRosterViewModel[] = await this.getRostersAsync(leagueId)
        const roster = rosters.find(r => r.roster_id === rosterId)

        return roster;
    }

    async getLeagueUsersAsync(leagueId: string): Promise<LeagueUsersViewModel[]> {
        const response = await this.sleeper.get(`${this.leaguePath}/${leagueId}/users`)
        const leagueUsers = response.data;

        return leagueUsers;
    }

    async getMatchupsAsync(leagueId: string, week: number): Promise<LeagueMatchupsViewModel[]> {
        const response = await this.sleeper.get(`${this.leaguePath}/${leagueId}/matchups/${week}`)
        const matchups = response.data;

        return matchups;
    }

    async getFullPlayoffsBracketAsync(leagueId: string): Promise<PlayoffBracketViewModel[]> {
        const winnersBracket = await this.getWinnersBracketAsync(leagueId);
        const losersBracket = await this.getLosersBracketAsync(leagueId);
        const fullBracket = winnersBracket.concat(losersBracket);

        return fullBracket;
    }

    async getWinnersBracketAsync(leagueId: string): Promise<PlayoffBracketViewModel[]> {
        const response = await this.sleeper.get(`${this.leaguePath}/${leagueId}/winners_bracket`);
        const winnersBracket = response.data;

        return winnersBracket;
    }

    async getLosersBracketAsync(leagueId: string): Promise<PlayoffBracketViewModel[]> {
        const response = await this.sleeper.get(`${this.leaguePath}/${leagueId}/losers_bracket`);
        const losersBracket = response.data;

        return losersBracket;
    }

    async getTransactionsAsync(leagueId: string, week: number): Promise<LeagueTransactionsViewModel[]> {
        const response = await this.sleeper.get(`${this.leaguePath}/${leagueId}/transactions/${week}`);
        const transactions = response.data;

        return transactions;
    }

    async getDraftsAsync(leagueId: string): Promise<DraftViewModel[]> {
        const response = await this.sleeper.get(`${this.leaguePath}/${leagueId}/drafts`);
        const drafts = response.data;

        return drafts;
    }

    async getMessagesAsync(leagueId: string): Promise<LeagueMessage[]> {
        const response = await this.sleeperGraphQL.post(this.graphQLPath, {
            query: `
                query messages {
                        messages(parent_id: "${leagueId}") {
                            attachment
                            parent_id
                        }
                    }
                `
        });
        const messages: LeagueMessage[] = response.data.data.messages;

        return messages;
    }

    async getLeagueReport(leagueId: string): Promise<LeagueReport> {
        const messages = await this.getMessagesAsync(leagueId);
        const leagueReport: LeagueReport = messages.find(m => m.attachment?.type === "league_report").attachment.data;

        return leagueReport;
    }
}