import { DraftViewModel } from "../models/drafts/DraftViewModel";
import { PickViewModel } from "../models/drafts/PickViewModel";
import { TradedPickViewModel } from "../models/drafts/TradedPickViewModel";
import { BaseClient } from "./BaseClient";

export class DraftClient extends BaseClient {
    private readonly draftPath: string;

    constructor() {
        super();

        this.draftPath = '/v1/draft';
    }

    async getDraftAsync(draftId: string): Promise<DraftViewModel> {
        const response = await this.sleeper.get(`${this.draftPath}/${draftId}`);
        const draft = response.data;
        
        return draft;
    }

    async getPicksInDraft(draftId: string): Promise<PickViewModel[]> {
        const response = await this.sleeper.get(`${this.draftPath}/${draftId}/picks`);
        const picks = response.data;

        return picks;
    }

    async getTradedPicksInDraft(draftId: string): Promise<TradedPickViewModel[]> {
        const response = await this.sleeper.get(`${this.draftPath}/${draftId}/traded_picks`);
        const tradedPicks = response.data;

        return tradedPicks;
    }
}