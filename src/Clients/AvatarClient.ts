import Axios from 'axios';
import * as  Path from 'path';
import * as Fs from 'fs';

import { BaseClient } from './BaseClient';

export class AvatarClient extends BaseClient {
    private readonly requestUrl: string;
    private readonly outputDirectory: string;
    constructor() {
        super();

        this.requestUrl = 'https://sleepercdn.com/avatars';
        this.outputDirectory = 'images';
    }

    async getProfilePictureAsync(avatarId: string, fullSize: boolean = false): Promise<any> {
        const fileName = `${avatarId}-${fullSize ? 'full' : 'thumb'}.png`
        const path = Path.resolve(this.outputDirectory, fileName);
        if (!Fs.existsSync(this.outputDirectory)) {
            Fs.mkdirSync(this.outputDirectory);
        }
        const writer = Fs.createWriteStream(path);

        const response = await Axios({
            url: `${this.requestUrl}/${fullSize ? '': 'thumbs/'}${avatarId}`,
            method: 'GET',
            responseType: 'stream'
        })

        response.data.pipe(writer);

        new Promise((resolve, reject) => {
            writer.on('finish', resolve);
            writer.on('error', reject);
        })

        return fileName;
    }
}