import { BaseClient } from './BaseClient';
import { UserViewModel } from '../models/users/UserViewModel';

export class UserClient extends BaseClient {
    private readonly userPath: string;
    constructor() {
        super();

        this.userPath = '/v1/user';
    }

    async getAsync(userIdentifier: string): Promise<UserViewModel> {
        const response = await this.sleeper.get(`${this.userPath}/${userIdentifier}`);
        const user = response.data;

        return user;
    }
}