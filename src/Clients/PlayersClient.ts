import * as Fs from 'fs';
import * as Path from 'path';
import { PlayerViewModel } from '../models/players/PlayerViewModel';

import { PlayersViewModel } from '../models/players/PlayersViewModel';
import { TrendingType } from '../models/players/TrendingType';
import { TrendingViewModel } from '../models/players/TrendingViewModel';
import { BaseClient } from "./BaseClient";

export class PlayersClient extends BaseClient {
    private readonly playerUrl: string;
    private readonly outputDirectory: string;

    constructor() {
        super();

        this.playerUrl = '/v1/players';
        this.outputDirectory = 'data';
    }

    async getAllPlayersAsync(): Promise<PlayerViewModel[]> {
        const response = await this.sleeper.get(`${this.playerUrl}/lcs`);
        const playersViewModel: PlayersViewModel = response.data;
        const players: PlayerViewModel[] = await PlayersViewModel.toArrayAsync(playersViewModel)

        return players;
    }

    async getTrendingPlayersAsync(trendingType: TrendingType, lookbackHours: number = 24, limit: number = 25): Promise<TrendingViewModel[]> {
        const response = await this.sleeper.get(`${this.playerUrl}/lcs/trending/${trendingType}?lookback_hours=${lookbackHours}&limit=${limit}`);
        const trendingPlayers = response.data;

        return trendingPlayers;
    }
}