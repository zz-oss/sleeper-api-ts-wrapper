import axios, { AxiosInstance } from "axios";

export class BaseClient {
    protected readonly sleeper: AxiosInstance;
    protected readonly sleeperGraphQL: AxiosInstance;
    private readonly graphQLAuth: string;

    constructor() {
        this.graphQLAuth = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdmF0YXIiOiJiNmY3ZWVlOWMwMDBiMWQ0MTcxNmViN2ZhODYwODA0MiIsImRpc3BsYXlfbmFtZSI6IlNhYXZ5MSIsImV4cCI6MTY0NTU1MTcxMywiaXNfYm90IjpudWxsLCJpc19tYXN0ZXIiOm51bGwsInJlYWxfbmFtZSI6bnVsbCwidXNlcl9pZCI6NjU2NjQ4OTE2MzMwMTg4ODAwfQ.TRdzN1yielbVSYujkZirV4_iTlkKlevHm0bijR2Rmn4';

        this.sleeper = axios.create({
            timeout: 1000,
            baseURL: 'https://api.sleeper.app'
        });

        this.sleeperGraphQL = axios.create({
            timeout: 1000,
            baseURL: 'https://sleeper.app/graphql',
            headers: { 'Authorization': this.graphQLAuth}
        })
    }
}