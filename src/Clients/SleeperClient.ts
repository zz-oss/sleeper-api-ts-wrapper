import { AvatarClient } from './AvatarClient';
import { UserClient } from './UserClient';
import { LeagueClient } from "./LeagueClient";
import { DraftClient } from './DraftClient';
import { PlayersClient } from './PlayersClient';

export class SleeperClient {
    public readonly user: UserClient;
    public readonly avatar: AvatarClient;
    public readonly league: LeagueClient;
    public readonly draft: DraftClient;
    public readonly players: PlayersClient;
    
    constructor() {
        this.user = new UserClient();
        this.avatar = new AvatarClient();
        this.league = new LeagueClient();
        this.draft = new DraftClient();
        this.players = new PlayersClient();
    }
}