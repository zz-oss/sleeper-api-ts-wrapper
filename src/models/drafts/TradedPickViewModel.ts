export class TradedPickViewModel {
    season: string;
    round: number;
    roster_id: number;
    previous_owner: number;
    owner_id: number;
}