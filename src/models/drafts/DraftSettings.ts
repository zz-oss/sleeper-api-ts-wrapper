export class DraftSettings {
    teams: number;
    slots_top: number;
    slots_sup: number;
    slots_mid: number;
    slots_jun: number;
    slots_adc: number;
    rounds: number;
    reversal_round: number;
    positions_limit_top: number;
    positions_limit_sup: number;
    positions_limit_mid: number;
    positions_limit_jun: number;
    positions_limit_adc: number;
    pick_timer: number;
    cpu_autopick: number;
}
