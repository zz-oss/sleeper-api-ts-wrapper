export class PickViewModel {
    round: number;
    roster_id: number;
    player_id: string;
    picked_by: string;
    pick_no: number;
    metadata: any;
    is_keeper: boolean;
    draft_slot: number;
    draft_id: number;
}