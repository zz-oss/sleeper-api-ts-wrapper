import { DraftSettings } from "./DraftSettings";

export class DraftViewModel {
    type: string;
    status: string;
    start_time: number;
    sport: string;
    slot_to_roster_id: any;
    settings: DraftSettings;
    season_type: string;
    season: string;
    metadata: any;
    league_id: string;
    last_picked: number;
    last_message_time: number;
    last_message_id: string;
    draft_order: any;
    draft_id: string;
    creators: string[];
    created: number;
}