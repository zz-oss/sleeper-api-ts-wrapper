
export class PlayerResult {
    roster_id: number;
    roster_id_2: number;
    position: string;
    points: number;
    player_id: string;
    pick: string;
    match_id: number;
    hero_id: string;
    game_id: string;
    base_points: number;
    ban: number;
}
