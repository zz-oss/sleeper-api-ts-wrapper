export class SingleGameResult {
    roster_id: number;
    points: number;
    base_points: number;
}
