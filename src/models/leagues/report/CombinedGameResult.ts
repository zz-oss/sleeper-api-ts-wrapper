export class CombinedGameResult {
    roster_id_2: number;
    roster_id_1: number;
    points_2: number;
    points_1: number;
}
