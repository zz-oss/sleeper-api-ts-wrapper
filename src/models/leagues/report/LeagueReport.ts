import { CombinedGameResult } from "./CombinedGameResult";
import { PlayerResult } from "./PlayerResult";
import { Scorer } from "./Scorer";
import { SingleGameResult } from "./SingleGameResult";
import { Star } from "./Star";

export class LeagueReport {
    week: number;
    top_scorers: Scorer[];
    stars: Star[];
    sleeper_picks: string[];
    sleeper_bans: string[];
    roster_user_map: any; // this is a generated object based on how many teams are in the league so i can't create a class for it
    nailbiter: CombinedGameResult;
    lowest_winner: CombinedGameResult;
    lowest_scorer: SingleGameResult;
    highest_scorer: SingleGameResult;
    highest_loser: CombinedGameResult;
    blowout: CombinedGameResult;
    best_pick: PlayerResult;
    best_ban: PlayerResult;
}

