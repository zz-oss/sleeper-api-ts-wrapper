import { PlayerResult } from "./PlayerResult";

export class Star {
    roster_id: number;
    position: string;
    points: number;
    player_id: string;
    games: PlayerResult[];
}
