export class RosterSettings {
    wins: number;
    waiver_position: number;
    waiver_budget_used: number;
    total_picks: number;
    total_moves: number;
    total_bans: number;
    ties: number;
    ppts_decimal: number;
    ppts: number;
    losses: number;
    fpts_decimal: number;
    fpts_against_decimal: number;
    fpts_against: number;
    fpts: number;
    correct_picks: number;
    correct_bans: number;
}
