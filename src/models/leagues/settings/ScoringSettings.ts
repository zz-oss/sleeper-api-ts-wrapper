export class ScoringSettings {
    sdb: number; // solo drake bonus
    pick_bonus: number;
    lsk: number; // lane solo kill
    kill: number;
    ft: number; // first turret
    fed: number; // first elder drake
    fdk: number; // first drake kill
    fbk: number; // first baron kill
    fb: number; // first blood
    death: number;
    cs15: number; // cs diff at 15 mins
    cs: number; // creep score (minions killed)
    ban_penalty: number;
    assist: number;

}