import { MessageAttachment } from "./MessageAttachment";
import { LeagueSettings } from "./settings/LeagueSettings";
import { RosterPositions } from "./RosterPositions";
import { ScoringSettings } from "./settings/ScoringSettings";

export class LeagueViewModel {
    total_rosters: number;
    status: string;
    sport: string;
    shard: number;
    settings: LeagueSettings;
    season_type: string;
    season: string;
    scoring_settings: ScoringSettings;
    roster_positions: RosterPositions[];
    previous_league_id: string;
    name: string;
    metadata: any;
    loser_bracket_id: string;
    league_id: string;
    last_read_id: string;
    last_pinned_message_id: string;
    last_message_time: number;
    last_message_text_map: any;
    last_message_text: string;
    last_message_id: string;
    last_message_attachment: MessageAttachment;
    last_author_is_bot: boolean;
    last_author_id: string;
    last_author_display_name: string;
    last_author_avatar: string;
    group_id: string;
    draft_id: string;
    company_id: string;
    bracket_id: string;
    avatar: string;
}
