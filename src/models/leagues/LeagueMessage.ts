import { MessageAttachment } from "./MessageAttachment";

export class LeagueMessage {
    parent_id: string;
    attachment: MessageAttachment;
}