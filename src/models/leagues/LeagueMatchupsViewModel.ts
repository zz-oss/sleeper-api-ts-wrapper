export class LeagueMatchupsViewModel {
    starter_points: number[];
    starters: string[];
    roster_id: string;
    points: number;
    players: string[];
    matchup_id: string;
    custom_points: any;
}