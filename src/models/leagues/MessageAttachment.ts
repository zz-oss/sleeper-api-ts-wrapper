export class MessageAttachment {
    type: string;
    data: any;
}
