export class LeagueUsersViewModel {
    user_id: string;
    settings: any; // TODO
    metadata: any;
    league_id: string;
    is_owner: boolean;
    is_bot: boolean;
    display_name: string;
    avatar: string;
}