import { RosterSettings } from "./settings/RosterSettings";

export class LeagueRosterViewModel {
    taxi: string;
    starters: string[];
    settings: RosterSettings;
    roster_id: number;
    reserve: string;
    players: string[];
    player_map: any;
    owner_id: string;
    metadata: any;
    league_id: string;
    co_owners: string[];
}