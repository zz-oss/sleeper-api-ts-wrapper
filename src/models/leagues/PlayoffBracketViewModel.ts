export class PlayoffBracketViewModel {
    r: number; // round
    m: number; // match id
    t1: number; // roster_id of a team in this matchup OR {w: 1} which means winner of match id 1
    t2: number; // roster_id of the other team in this matchcup or {l: 1} which means loser of match id 1
    w: number; // roster_id of the winning team, if the match has been played
    l: number; // roster_id of the losing team, if the match has been played
    t1_from: any;
    t2_from: any;
}