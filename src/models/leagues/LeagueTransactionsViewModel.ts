export class LeagueTransactionsViewModel {
    wavier_budet: any[];
    type: string;
    transaction_id: string;
    status_updated: number;
    status: string;
    roster_ids: number[];
    metadata: any;
    leg: number;
    drops: any;
    draft_picks: any[];
    creator: string;
    created: number;
    consenter_ids: string[];
    adds: any;
}