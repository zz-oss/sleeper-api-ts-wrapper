import { PlayerViewModel } from "./PlayerViewModel";

export class PlayersViewModel {
    1000: PlayerViewModel;
    1001: PlayerViewModel;
    1002: PlayerViewModel;
    1003: PlayerViewModel;
    1004: PlayerViewModel;
    1005: PlayerViewModel;
    1006: PlayerViewModel;
    1007: PlayerViewModel;
    1008: PlayerViewModel;
    1009: PlayerViewModel;
    1010: PlayerViewModel;
    1011: PlayerViewModel;
    1012: PlayerViewModel;
    1013: PlayerViewModel;
    1014: PlayerViewModel;
    1015: PlayerViewModel;
    1016: PlayerViewModel;
    1017: PlayerViewModel;
    1018: PlayerViewModel;
    1019: PlayerViewModel;
    1020: PlayerViewModel;
    1021: PlayerViewModel;
    1022: PlayerViewModel;
    1023: PlayerViewModel;
    1024: PlayerViewModel;
    1025: PlayerViewModel;
    1026: PlayerViewModel;
    1027: PlayerViewModel;
    1028: PlayerViewModel;
    1029: PlayerViewModel;
    1030: PlayerViewModel;
    1031: PlayerViewModel;
    1032: PlayerViewModel;
    1033: PlayerViewModel;
    1034: PlayerViewModel;
    1035: PlayerViewModel;
    1036: PlayerViewModel;
    1037: PlayerViewModel;
    1038: PlayerViewModel;
    1039: PlayerViewModel;
    1040: PlayerViewModel;
    1041: PlayerViewModel;
    1042: PlayerViewModel;
    1043: PlayerViewModel;
    1044: PlayerViewModel;
    1045: PlayerViewModel;
    1046: PlayerViewModel;
    1047: PlayerViewModel;
    1048: PlayerViewModel;
    1049: PlayerViewModel;
    1050: PlayerViewModel;
    1051: PlayerViewModel;
    1052: PlayerViewModel;
    1053: PlayerViewModel;
    1054: PlayerViewModel;
    1055: PlayerViewModel;
    1056: PlayerViewModel;
    1057: PlayerViewModel;
    1058: PlayerViewModel;
    1059: PlayerViewModel;
    1060: PlayerViewModel;
    1061: PlayerViewModel;
    1062: PlayerViewModel;
    1063: PlayerViewModel;
    1064: PlayerViewModel;
    1065: PlayerViewModel;
    1066: PlayerViewModel;
    1067: PlayerViewModel;
    1068: PlayerViewModel;
    1069: PlayerViewModel;
    1070: PlayerViewModel;
    1071: PlayerViewModel;
    1072: PlayerViewModel;
    1073: PlayerViewModel;
    1074: PlayerViewModel;
    1075: PlayerViewModel;
    1076: PlayerViewModel;
    1077: PlayerViewModel;
    1078: PlayerViewModel;
    1079: PlayerViewModel;
    1080: PlayerViewModel;
    1081: PlayerViewModel;
    1082: PlayerViewModel;
    1083: PlayerViewModel;
    1084: PlayerViewModel;
    1085: PlayerViewModel;
    1086: PlayerViewModel;
    1087: PlayerViewModel;
    1088: PlayerViewModel;
    1089: PlayerViewModel;
    1090: PlayerViewModel;
    1091: PlayerViewModel;
    1092: PlayerViewModel;
    1093: PlayerViewModel;
    1094: PlayerViewModel;
    1095: PlayerViewModel;
    1096: PlayerViewModel;
    1097: PlayerViewModel;
    1098: PlayerViewModel;
    1099: PlayerViewModel;
    
    static async toArrayAsync(playersViewModel: PlayersViewModel): Promise<PlayerViewModel[]> {
        const players: PlayerViewModel[] = [];
        const playerKeys = Object.keys(playersViewModel)
        for (let pk in playerKeys) {
            const playerId: number = parseInt(pk) + 1000
            if (playerId >= 1000 && playerId <= 1099) {
                players.push(playersViewModel[playerId])
            }
        }

        return players
    }
}