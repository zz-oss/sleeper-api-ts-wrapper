export class PlayerViewModel {
    position: string;
    search_full_name: string;
    status: string;
    pandascore_id: number;
    first_name: string;
    hashtag: string;
    player_id: number;
    sport: string;
    age: number;
    search_last_name: DOMMatrixInit;
    fantasy_positions: string[];
    team: string;
    last_name: string;
    birth_date: Date;
    metadata: any;
    search_first_name: string;
    full_name: string;
}
