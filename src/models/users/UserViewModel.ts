export class UserViewModel {
    username: string;
    user_id: string;
    display_name: string;
    avatar: string;
}